import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Incomes extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  items: string;

  @property({
    type: 'string',
    required: true,
  })
  createdAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Incomes>) {
    super(data);
  }
}

export interface IncomesRelations {
  // describe navigational properties here
}

export type IncomesWithRelations = Incomes & IncomesRelations;
