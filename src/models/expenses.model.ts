import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Expenses extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  items: string;

  @property({
    type: 'string',
    required: true,
  })
  createdAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Expenses>) {
    super(data);
  }
}

export interface ExpensesRelations {
  // describe navigational properties here
}

export type ExpensesWithRelations = Expenses & ExpensesRelations;
