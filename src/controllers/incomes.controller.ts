import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Incomes} from '../models';
import {IncomesRepository} from '../repositories';

export class IncomesController {
  constructor(
    @repository(IncomesRepository)
    public incomesRepository : IncomesRepository,
  ) {}

  @post('/incomes')
  @response(200, {
    description: 'Incomes model instance',
    content: {'application/json': {schema: getModelSchemaRef(Incomes)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Incomes, {
            title: 'NewIncomes',
            exclude: ['id'],
          }),
        },
      },
    })
    incomes: Omit<Incomes, 'id'>,
  ): Promise<Incomes> {
    return this.incomesRepository.create(incomes);
  }

  @get('/incomes/count')
  @response(200, {
    description: 'Incomes model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Incomes) where?: Where<Incomes>,
  ): Promise<Count> {
    return this.incomesRepository.count(where);
  }

  @get('/incomes')
  @response(200, {
    description: 'Array of Incomes model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Incomes, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Incomes) filter?: Filter<Incomes>,
  ): Promise<Incomes[]> {
    return this.incomesRepository.find(filter);
  }

  @patch('/incomes')
  @response(200, {
    description: 'Incomes PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Incomes, {partial: true}),
        },
      },
    })
    incomes: Incomes,
    @param.where(Incomes) where?: Where<Incomes>,
  ): Promise<Count> {
    return this.incomesRepository.updateAll(incomes, where);
  }

  @get('/incomes/{id}')
  @response(200, {
    description: 'Incomes model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Incomes, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Incomes, {exclude: 'where'}) filter?: FilterExcludingWhere<Incomes>
  ): Promise<Incomes> {
    return this.incomesRepository.findById(id, filter);
  }

  @patch('/incomes/{id}')
  @response(204, {
    description: 'Incomes PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Incomes, {partial: true}),
        },
      },
    })
    incomes: Incomes,
  ): Promise<void> {
    await this.incomesRepository.updateById(id, incomes);
  }

  @put('/incomes/{id}')
  @response(204, {
    description: 'Incomes PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() incomes: Incomes,
  ): Promise<void> {
    await this.incomesRepository.replaceById(id, incomes);
  }

  @del('/incomes/{id}')
  @response(204, {
    description: 'Incomes DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.incomesRepository.deleteById(id);
  }
}
