import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongoDataSource} from '../datasources';
import {Incomes, IncomesRelations} from '../models';

export class IncomesRepository extends DefaultCrudRepository<
  Incomes,
  typeof Incomes.prototype.id,
  IncomesRelations
> {
  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
  ) {
    super(Incomes, dataSource);
  }
}
