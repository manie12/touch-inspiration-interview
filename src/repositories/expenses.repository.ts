import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongoDataSource} from '../datasources';
import {Expenses, ExpensesRelations} from '../models';

export class ExpensesRepository extends DefaultCrudRepository<
  Expenses,
  typeof Expenses.prototype.id,
  ExpensesRelations
> {
  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
  ) {
    super(Expenses, dataSource);
  }
}
